﻿CREATE DATABASE application;
CREATE TABLE `persona` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `genero` varchar(1) DEFAULT NULL,
  `edad` int DEFAULT NULL,
  `identificacion` varchar(20) NOT NULL,
  `direccion` varchar(45) DEFAULT NULL,
  `telefono` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `identificacion_UNIQUE` (`identificacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
CREATE TABLE `cliente` (
  `id` int NOT NULL AUTO_INCREMENT,
  `contrasena` varchar(45) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT '1',
  `persona_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_clientexpersona_idx` (`persona_id`),
  CONSTRAINT `fk_clientexpersona` FOREIGN KEY (`persona_id`) REFERENCES `persona` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
CREATE TABLE `cuenta` (
  `id` int NOT NULL AUTO_INCREMENT,
  `numero_cuenta` varchar(45) NOT NULL,
  `tipo_cuenta` varchar(45) DEFAULT NULL,
  `saldo_inicial` double DEFAULT '0',
  `saldo_actual` double DEFAULT '0',
  `estado` tinyint(1) DEFAULT '1',
  `cliente_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `numero_cuenta_UNIQUE` (`numero_cuenta`),
  KEY `fk_cuentexcliente_idx` (`cliente_id`),
  CONSTRAINT `fk_cuentexcliente` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
CREATE TABLE `movimiento` (
  `id` int NOT NULL AUTO_INCREMENT,
  `fecha` datetime DEFAULT CURRENT_TIMESTAMP,
  `movimiento` varchar(100) DEFAULT NULL,
  `tipo_movimiento` varchar(1) DEFAULT NULL,
  `valor` double DEFAULT NULL,
  `saldo` double DEFAULT NULL,
  `cuenta_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_movimientoxcuenta_idx` (`cuenta_id`),
  CONSTRAINT `fk_movimientoxcuenta` FOREIGN KEY (`cuenta_id`) REFERENCES `cuenta` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;