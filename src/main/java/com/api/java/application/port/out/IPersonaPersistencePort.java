package com.api.java.application.port.out;

import com.api.java.domain.models.PersonaModel;

public interface IPersonaPersistencePort {
    PersonaModel get(Integer id);
    PersonaModel create(PersonaModel personaModel);
    PersonaModel update(PersonaModel personaModel);
    void delete(PersonaModel personaModel);
}
