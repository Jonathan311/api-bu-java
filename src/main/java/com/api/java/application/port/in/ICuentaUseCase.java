package com.api.java.application.port.in;

import com.api.java.domain.models.CuentaModel;

public interface ICuentaUseCase {
    CuentaModel get(Integer id);
    CuentaModel create(CuentaModel cuentaModel);
    CuentaModel update(CuentaModel cuentaModel);
    void delete(Integer id);
}
