package com.api.java.application.port.out;

import com.api.java.domain.models.ClienteModel;

public interface IClientePersistencePort {
    ClienteModel get(Integer id);
    ClienteModel create(ClienteModel clienteModel);
    ClienteModel update(ClienteModel clienteModel);
    void delete(ClienteModel clienteModel);
}
