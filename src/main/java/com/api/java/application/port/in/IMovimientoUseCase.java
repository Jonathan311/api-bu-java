package com.api.java.application.port.in;

import com.api.java.domain.models.MovimientoModel;
import com.api.java.domain.models.ReportModel;

import java.util.Date;
import java.util.List;

public interface IMovimientoUseCase {
    MovimientoModel get(Integer id);
    MovimientoModel create(MovimientoModel movimientoModel);
    MovimientoModel update(MovimientoModel movimientoModel);
    void delete(Integer id);
    List<ReportModel> report(Date start, Date end, Integer clienteId);
}
