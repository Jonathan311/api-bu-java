package com.api.java.application.port.out;

import com.api.java.domain.models.CuentaModel;

public interface ICuentaPersistencePort {
    CuentaModel get(Integer id);
    CuentaModel create(CuentaModel cuentaModel);
    CuentaModel update(CuentaModel cuentaModel);
    void delete(CuentaModel cuentaModel);
}
