package com.api.java.application.port.in;

import com.api.java.domain.models.ClienteModel;

public interface IClienteUseCase {
    ClienteModel get(Integer id);
    ClienteModel create(ClienteModel clienteModel);
    ClienteModel update(ClienteModel clienteModel);
    void delete(Integer id);
}
