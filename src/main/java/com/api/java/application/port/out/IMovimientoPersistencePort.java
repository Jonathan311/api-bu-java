package com.api.java.application.port.out;

import com.api.java.domain.models.MovimientoModel;

import java.util.Date;
import java.util.List;

public interface IMovimientoPersistencePort {
    MovimientoModel get(Integer id);
    MovimientoModel create(MovimientoModel movimientoModel);
    MovimientoModel update(MovimientoModel movimientoModel);
    void delete(MovimientoModel movimientoModel);
    List<MovimientoModel> report(Date start, Date end, Integer cliente);
}
