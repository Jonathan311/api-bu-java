package com.api.java.infrastructure.in.rest.command.response;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@Tag(name = "MovimientoDto")
public class MovimientoDto {
    private Integer id;
    private Date fecha;
    private String movimiento;
    private String tipoMovimiento;
    private Double valor;
    private Double saldo;
    private CuentaDto cuenta;
}
