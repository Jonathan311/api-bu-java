package com.api.java.infrastructure.in.rest.mapper;

import com.api.java.domain.models.MovimientoModel;
import com.api.java.domain.models.ReportModel;
import com.api.java.infrastructure.in.rest.command.request.movimiento.MovimientoCreateRequest;
import com.api.java.infrastructure.in.rest.command.request.movimiento.MovimientoUpdateRequest;
import com.api.java.infrastructure.in.rest.command.response.MovimientoDto;
import com.api.java.infrastructure.in.rest.command.response.ReportDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface MovimientoMapper {
    @Mapping(target="cuenta.id", source="cuentaid")
    MovimientoModel toDomain(MovimientoCreateRequest source);
    @Mapping(target="cuenta.id", source="cuentaid")
    MovimientoModel toDomain(MovimientoUpdateRequest source);
    MovimientoDto toDto(MovimientoModel source);
    List<ReportDto> toDto(List<ReportModel> source);
}
