package com.api.java.infrastructure.in.rest.command.response;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Tag(name = "ClienteDto")
public class CuentaDto {
    private Integer id;
    private String numeroCuenta;
    private String tipoCuenta;
    private Double saldoInicial;
    private Double saldoActual;
    private boolean estado;
    private ClienteDto cliente;
}
