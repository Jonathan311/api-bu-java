package com.api.java.infrastructure.in.rest.command.request.cuenta;

import com.api.java.commons.SelfValidating;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Slf4j
@Data
@Tag(name = "CuentaUpdateRequest", description = "Solicitud de actualizar cuenta")
public class CuentaUpdateRequest extends SelfValidating<CuentaUpdateRequest> {

    @Schema(description = "ID de la cuenta", required = true)
    @NotNull(message = "{id.null}")
    private Integer id;

    @Schema(description = "Numero de cuenta", required = true)
    @NotNull(message = "{numeroCuenta.null}")
    @Size(min = 1, max = 45, message = "{numeroCuenta.size}")
    private String numeroCuenta;

    @Schema(description = "Tipo de cuenta", required = true)
    @NotNull(message = "{tipoCuenta.null}")
    @Size(min = 1, max = 45, message = "{tipoCuenta.size}")
    private String tipoCuenta;

    @Schema(description = "saldoInicial", required = true)
    @NotNull(message = "{saldoInicial.null}")
    private Double saldoInicial;

    @Schema(description = "Estado de la cuenta", required = true)
    @NotNull(message = "{estado.null}")
    private boolean estado;

    @Schema(description = "clienteid", required = true)
    @NotNull(message = "{clienteid.null}")
    private Integer clienteid;

    public CuentaUpdateRequest(Integer id, String numeroCuenta, String tipoCuenta, Double saldoInicial, boolean estado, Integer clienteid) {
        this.id = id;
        this.numeroCuenta = numeroCuenta;
        this.tipoCuenta = tipoCuenta;
        this.saldoInicial = saldoInicial;
        this.estado = estado;
        this.clienteid = clienteid;
        this.validateSelf();
    }
}
