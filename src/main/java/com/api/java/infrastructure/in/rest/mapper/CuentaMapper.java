package com.api.java.infrastructure.in.rest.mapper;

import com.api.java.domain.models.CuentaModel;
import com.api.java.infrastructure.in.rest.command.request.cuenta.CuentaCreateRequest;
import com.api.java.infrastructure.in.rest.command.request.cuenta.CuentaUpdateRequest;
import com.api.java.infrastructure.in.rest.command.response.CuentaDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CuentaMapper {
    @Mapping(target="cliente.id", source="clienteid")
    CuentaModel toDomain(CuentaCreateRequest source);
    @Mapping(target="cliente.id", source="clienteid")
    CuentaModel toDomain(CuentaUpdateRequest source);
    CuentaDto toDto(CuentaModel source);
}
