package com.api.java.infrastructure.in.rest.mapper;

import com.api.java.infrastructure.in.rest.command.request.cliente.ClienteCreateRequest;
import com.api.java.domain.models.ClienteModel;
import com.api.java.infrastructure.in.rest.command.request.cliente.ClienteUpdateRequest;
import com.api.java.infrastructure.in.rest.command.response.ClienteDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ClienteMapper {
    @Mapping(target="persona.nombre", source="nombre")
    @Mapping(target="persona.genero", source="genero")
    @Mapping(target="persona.edad", source="edad")
    @Mapping(target="persona.identificacion", source="identificacion")
    @Mapping(target="persona.direccion", source="direccion")
    @Mapping(target="persona.telefono", source="telefono")
    ClienteModel toDomain(ClienteCreateRequest source);
    @Mapping(target="persona.nombre", source="nombre")
    @Mapping(target="persona.genero", source="genero")
    @Mapping(target="persona.edad", source="edad")
    @Mapping(target="persona.identificacion", source="identificacion")
    @Mapping(target="persona.direccion", source="direccion")
    @Mapping(target="persona.telefono", source="telefono")
    ClienteModel toDomain(ClienteUpdateRequest source);
    ClienteDto toDto(ClienteModel source);
}
