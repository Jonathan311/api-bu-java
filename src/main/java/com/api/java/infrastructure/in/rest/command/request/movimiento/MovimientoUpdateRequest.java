package com.api.java.infrastructure.in.rest.command.request.movimiento;

import com.api.java.commons.SelfValidating;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Slf4j
@Data
@Tag(name = "MovimientoCreateRequest", description = "Solicitud de crear movimiento")
public class MovimientoUpdateRequest extends SelfValidating<MovimientoUpdateRequest> {

    @Schema(description = "ID del movimiento", required = true)
    @NotNull(message = "{id.null}")
    private Integer id;

    @Schema(description = "Movimiento", required = true)
    @NotNull(message = "{movimiento.null}")
    @Size(min = 1, max = 100, message = "{movimiento.size}")
    private String movimiento;

    @Schema(description = "Tipo de movimiento", required = true)
    @Pattern(regexp = "D|C", message = "{tipoMovimiento.mask}")
    @NotNull(message = "{tipoMovimiento.null}")
    private String tipoMovimiento;

    @Schema(description = "valor", required = true)
    @NotNull(message = "{valor.null}")
    private Double valor;

    @Schema(description = "cuentaid", required = true)
    @NotNull(message = "{cuentaid.null}")
    private Integer cuentaid;

    public MovimientoUpdateRequest(String movimiento, Integer id, String tipoMovimiento, Double valor, Integer cuentaid) {
        this.id = id;
        this.movimiento = movimiento;
        this.tipoMovimiento = tipoMovimiento;
        this.valor = valor;
        this.cuentaid = cuentaid;
        this.validateSelf();
    }
}