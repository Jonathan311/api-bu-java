package com.api.java.infrastructure.in.rest.resources;

import com.api.java.application.port.in.IMovimientoUseCase;
import com.api.java.commons.Constants;
import com.api.java.commons.CustomResponse;
import com.api.java.commons.GlobalProperties;
import com.api.java.commons.helper.UtilsHelper;
import com.api.java.commons.logs.LogsHelper;
import com.api.java.commons.logs.enums.OperationType;
import com.api.java.domain.models.MovimientoModel;
import com.api.java.domain.models.ReportModel;
import com.api.java.exceptions.CustomError;
import com.api.java.infrastructure.in.rest.command.request.movimiento.MovimientoCreateRequest;
import com.api.java.infrastructure.in.rest.command.request.movimiento.MovimientoUpdateRequest;
import com.api.java.infrastructure.in.rest.command.response.MovimientoDto;
import com.api.java.infrastructure.in.rest.command.response.ReportDto;
import com.api.java.infrastructure.in.rest.mapper.MovimientoMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin(origins = Constants.APP_SAFE, methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
@RequestMapping(Constants.PROJECT_PATH + Constants.MOVIMIENTO_CONTROLLER_PATH)
@Tag(name = "Movimiento Controller", description = "Controlador de todos los metodos para la gestion de movimientos")
public class MovimientoController {

    @Autowired
    private GlobalProperties globalProperties;

    @Autowired
    private IMovimientoUseCase iMovimientoUseCase;

    @Autowired
    private MovimientoMapper movimientoMapper;

    @Autowired
    private CustomResponse customResponse;

    @ApiResponse(responseCode = "200", description = "Success", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomResponse.class))})
    @ApiResponse(responseCode = "400", description = "Bad Request", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomError.class))})

    @Operation(summary = "GetMovimiento", description = "Obtiene info del movimiento")
    @GetMapping(value = Constants.MOVIMIENTO_METHOD_FIND_PATH)
    public ResponseEntity<?> getMovimiento(@Valid @PathVariable("movimientoid") Integer movimientoid) {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(null, OperationType.MOVIMIENTO_GET_REQUEST, globalProperties);

        final MovimientoModel data = iMovimientoUseCase.get(movimientoid);
        final MovimientoDto movimientoDto = movimientoMapper.toDto(data);
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(movimientoDto);

        LogsHelper.getLogEnd(responseSuccess, OperationType.MOVIMIENTO_GET_REQUEST, globalProperties);
        return ResponseEntity.ok().body(responseSuccess);
    }

    @Operation(summary = "CreateMovimiento", description = "Registra un nuevo Movimiento")
    @PostMapping(value = Constants.MOVIMIENTO_METHOD_POST_PATH)
    public ResponseEntity<CustomResponse> createMovimiento(@RequestBody MovimientoCreateRequest movimientoCreateRequest) {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(movimientoCreateRequest, OperationType.MOVIMIENTO_POST_REQUEST, globalProperties);

        MovimientoModel domain = movimientoMapper.toDomain(movimientoCreateRequest);
        domain = iMovimientoUseCase.create(domain);
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(domain);

        LogsHelper.getLogEnd(responseSuccess, OperationType.MOVIMIENTO_POST_REQUEST, globalProperties);
        return ResponseEntity.ok().body(responseSuccess);
    }

    @Operation(summary = "UpdateMovimiento", description = "Actualiza un Movimiento")
    @PutMapping(value = Constants.MOVIMIENTO_METHOD_PUT_PATH)
    public ResponseEntity<CustomResponse> updateMovimiento(@RequestBody MovimientoUpdateRequest movimientoUpdateRequest) {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(movimientoUpdateRequest, OperationType.MOVIMIENTO_PUT_REQUEST, globalProperties);

        MovimientoModel domain = movimientoMapper.toDomain(movimientoUpdateRequest);
        domain = iMovimientoUseCase.update(domain);
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(domain);

        LogsHelper.getLogEnd(responseSuccess, OperationType.MOVIMIENTO_PUT_REQUEST, globalProperties);
        return ResponseEntity.ok().body(responseSuccess);
    }

    @Operation(summary = "DeleteMovimiento", description = "Elimina un Movimiento")
    @DeleteMapping(value = Constants.MOVIMIENTO_METHOD_DELETE_PATH)
    public ResponseEntity<CustomResponse> deleteMovimiento(@Valid @PathVariable("movimientoid") Integer movimientoid) {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(null, OperationType.MOVIMIENTO_DELETE_REQUEST, globalProperties);

        iMovimientoUseCase.delete(movimientoid);
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(null);

        LogsHelper.getLogEnd(responseSuccess, OperationType.MOVIMIENTO_DELETE_REQUEST, globalProperties);
        return ResponseEntity.ok().body(responseSuccess);
    }

    @Operation(summary = "GetMovimientos", description = "Obtiene info de los movimientos")
    @GetMapping(value = Constants.MOVIMIENTO_METHOD_REPORT_PATH)
    public ResponseEntity<?> getMovimientos(@RequestParam() Date start, @RequestParam() Date end, @RequestParam() Integer clienteId) {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(null, OperationType.MOVIMIENTO_REPORT_REQUEST, globalProperties);

        final List<ReportModel> data = iMovimientoUseCase.report(start, end, clienteId);
        final List<ReportDto> reportDto = movimientoMapper.toDto(data);
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(reportDto);

        LogsHelper.getLogEnd(responseSuccess, OperationType.MOVIMIENTO_REPORT_REQUEST, globalProperties);
        return ResponseEntity.ok().body(responseSuccess);
    }
}