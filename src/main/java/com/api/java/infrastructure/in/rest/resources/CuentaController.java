package com.api.java.infrastructure.in.rest.resources;

import com.api.java.application.port.in.ICuentaUseCase;
import com.api.java.commons.Constants;
import com.api.java.commons.CustomResponse;
import com.api.java.commons.GlobalProperties;
import com.api.java.commons.helper.UtilsHelper;
import com.api.java.commons.logs.LogsHelper;
import com.api.java.commons.logs.enums.OperationType;
import com.api.java.domain.models.CuentaModel;
import com.api.java.exceptions.CustomError;
import com.api.java.infrastructure.in.rest.command.request.cuenta.CuentaCreateRequest;
import com.api.java.infrastructure.in.rest.command.request.cuenta.CuentaUpdateRequest;
import com.api.java.infrastructure.in.rest.command.response.CuentaDto;
import com.api.java.infrastructure.in.rest.mapper.CuentaMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin(origins = Constants.APP_SAFE, methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
@RequestMapping(Constants.PROJECT_PATH + Constants.CUENTA_CONTROLLER_PATH)
@Tag(name = "Cuenta Controller", description = "Controlador de todos los metodos para la gestion de cuentas")
public class CuentaController {

    @Autowired
    private GlobalProperties globalProperties;

    @Autowired
    private ICuentaUseCase iCuentaUseCase;

    @Autowired
    private CuentaMapper cuentaMapper;

    @Autowired
    private CustomResponse customResponse;

    @ApiResponse(responseCode = "200", description = "Success", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomResponse.class))})
    @ApiResponse(responseCode = "400", description = "Bad Request", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomError.class))})

    @Operation(summary = "GetCuenta", description = "Obtiene info de la cuenta")
    @GetMapping(value = Constants.CUENTA_METHOD_FIND_PATH)
    public ResponseEntity<?> getCuenta(@Valid @PathVariable("cuentaid") Integer cuentaid) {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(null, OperationType.CUENTA_GET_REQUEST, globalProperties);

        final CuentaModel data = iCuentaUseCase.get(cuentaid);
        final CuentaDto cuentaDto = cuentaMapper.toDto(data);
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(cuentaDto);

        LogsHelper.getLogEnd(responseSuccess, OperationType.CUENTA_GET_REQUEST, globalProperties);
        return ResponseEntity.ok().body(responseSuccess);
    }

    @Operation(summary = "CreateCuenta", description = "Registra una nueva cuenta")
    @PostMapping(value = Constants.CUENTA_METHOD_POST_PATH)
    public ResponseEntity<CustomResponse> createCuenta(@RequestBody CuentaCreateRequest cuentaCreateRequest) {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(cuentaCreateRequest, OperationType.CUENTA_POST_REQUEST, globalProperties);

        CuentaModel domain = cuentaMapper.toDomain(cuentaCreateRequest);
        domain = iCuentaUseCase.create(domain);
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(domain);

        LogsHelper.getLogEnd(responseSuccess, OperationType.CUENTA_POST_REQUEST, globalProperties);
        return ResponseEntity.ok().body(responseSuccess);
    }

    @Operation(summary = "UpdateCuenta", description = "Actualiza una cuenta")
    @PutMapping(value = Constants.CUENTA_METHOD_PUT_PATH)
    public ResponseEntity<CustomResponse> updateCuenta(@RequestBody CuentaUpdateRequest cuentaUpdateRequest) {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(cuentaUpdateRequest, OperationType.CUENTA_PUT_REQUEST, globalProperties);

        CuentaModel domain = cuentaMapper.toDomain(cuentaUpdateRequest);
        domain = iCuentaUseCase.update(domain);
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(domain);

        LogsHelper.getLogEnd(responseSuccess, OperationType.CUENTA_PUT_REQUEST, globalProperties);
        return ResponseEntity.ok().body(responseSuccess);
    }

    @Operation(summary = "DeleteCuenta", description = "Elimina una cuenta")
    @DeleteMapping(value = Constants.CUENTA_METHOD_DELETE_PATH)
    public ResponseEntity<CustomResponse> deleteCuenta(@Valid @PathVariable("cuentaid") Integer cuentaid) {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(null, OperationType.CUENTA_DELETE_REQUEST, globalProperties);

        iCuentaUseCase.delete(cuentaid);
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(null);

        LogsHelper.getLogEnd(responseSuccess, OperationType.CUENTA_DELETE_REQUEST, globalProperties);
        return ResponseEntity.ok().body(responseSuccess);
    }
}