package com.api.java.infrastructure.in.rest.command.response;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Tag(name = "ReportDto")
public class ReportDto {
    private String fecha;
    private String cliente;
    private String numeroCuenta;
    private String tipo;
    private Double saldoInicial;
    private String estado;
    private Double movimiento;
    private Double saldoDisponible;
}
