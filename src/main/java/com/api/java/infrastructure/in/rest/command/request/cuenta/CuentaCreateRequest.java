package com.api.java.infrastructure.in.rest.command.request.cuenta;

import com.api.java.commons.SelfValidating;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Slf4j
@Data
@Tag(name = "CuentaCreateRequest", description = "Solicitud de crear cuenta")
public class CuentaCreateRequest extends SelfValidating<CuentaCreateRequest> {

    @Schema(description = "Numero de cuenta", required = true)
    @NotNull(message = "{numeroCuenta.null}")
    @Size(min = 1, max = 45, message = "{numeroCuenta.size}")
    private String numeroCuenta;

    @Schema(description = "Tipo de cuenta", required = true)
    @NotNull(message = "{tipoCuenta.null}")
    @Size(min = 1, max = 45, message = "{tipoCuenta.size}")
    private String tipoCuenta;

    @Schema(description = "saldoInicial", required = true)
    @NotNull(message = "{saldoInicial.null}")
    private Double saldoInicial;

    @Schema(description = "clienteid", required = true)
    @NotNull(message = "{clienteid.null}")
    private Integer clienteid;

    public CuentaCreateRequest(String numeroCuenta, String tipoCuenta, Double saldoInicial, Integer clienteid) {
        this.numeroCuenta = numeroCuenta;
        this.tipoCuenta = tipoCuenta;
        this.saldoInicial = saldoInicial;
        this.clienteid = clienteid;
        this.validateSelf();
    }
}