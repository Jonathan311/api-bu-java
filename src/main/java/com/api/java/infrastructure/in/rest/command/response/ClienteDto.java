package com.api.java.infrastructure.in.rest.command.response;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Tag(name = "ClienteDto")
public class ClienteDto {
    private Integer id;
    private String contrasena;
    private boolean estado;
    private PersonaDto persona;
}
