package com.api.java.infrastructure.in.rest.command.request.cliente;

import com.api.java.commons.SelfValidating;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Slf4j
@Data
@Tag(name = "ClienteUpdateRequest", description = "Solicitud de actualizar cliente")
public class ClienteUpdateRequest extends SelfValidating<ClienteUpdateRequest> {

    @Schema(description = "Nombre", required = true)
    @NotNull(message = "{nombre.null}")
    @Size(min = 1, max = 45, message = "{nombre.size}")
    private String nombre;

    @Schema(description = "Genero", required = true)
    @Pattern(regexp = "F|M", message = "{genero.mask}")
    @NotNull(message = "{genero.null}")
    private String genero;

    @Schema(description = "Edad", required = true)
    @NotNull(message = "{edad.null}")
    private Integer edad;

    @Schema(description = "Identificacion", required = true)
    @NotNull(message = "{identificacion.null}")
    @Size(min = 1, max = 20, message = "{identificacion.size}")
    private String identificacion;

    @Schema(description = "Direccion", required = true)
    @NotNull(message = "{direccion.null}")
    @Size(min = 1, max = 45, message = "{direccion.size}")
    private String direccion;

    @Schema(description = "Telefono", required = true)
    @NotNull(message = "{telefono.null}")
    @Size(min = 1, max = 15, message = "{telefono.size}")
    private String telefono;

    @Schema(description = "ID del cliente", required = true)
    @NotNull(message = "{id.null}")
    private Integer id;

    @Schema(description = "Estado del cliente", required = true)
    @NotNull(message = "{estado.null}")
    private boolean estado;

    @Schema(description = "Contraseña", required = true)
    @NotNull(message = "{contrasena.null}")
    @Size(min = 1, max = 45, message = "{contrasena.size}")
    private String contrasena;

    public ClienteUpdateRequest(String nombre, String genero, Integer edad, String identificacion, String direccion, String telefono, Integer id, boolean estado, String contrasena) {
        this.nombre = nombre;
        this.genero = genero;
        this.edad = edad;
        this.identificacion = identificacion;
        this.direccion = direccion;
        this.telefono = telefono;
        this.id = id;
        this.estado = estado;
        this.contrasena = contrasena;
        this.validateSelf();
    }
}