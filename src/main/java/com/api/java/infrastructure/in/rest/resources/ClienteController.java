package com.api.java.infrastructure.in.rest.resources;

import com.api.java.application.port.in.IClienteUseCase;
import com.api.java.commons.Constants;
import com.api.java.commons.CustomResponse;
import com.api.java.commons.GlobalProperties;
import com.api.java.commons.helper.UtilsHelper;
import com.api.java.commons.logs.LogsHelper;
import com.api.java.commons.logs.enums.OperationType;
import com.api.java.domain.models.ClienteModel;
import com.api.java.exceptions.CustomError;
import com.api.java.infrastructure.in.rest.command.request.cliente.ClienteCreateRequest;
import com.api.java.infrastructure.in.rest.command.request.cliente.ClienteUpdateRequest;
import com.api.java.infrastructure.in.rest.command.response.ClienteDto;
import com.api.java.infrastructure.in.rest.mapper.ClienteMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin(origins = Constants.APP_SAFE, methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
@RequestMapping(Constants.PROJECT_PATH + Constants.CLIENTE_CONTROLLER_PATH)
@Tag(name = "Cliente Controller", description = "Controlador de todos los metodos para la gestion de clientes")
public class ClienteController {

    @Autowired
    private GlobalProperties globalProperties;

    @Autowired
    private IClienteUseCase iClienteUseCase;

    @Autowired
    private ClienteMapper clienteMapper;

    @Autowired
    private CustomResponse customResponse;

    @ApiResponse(responseCode = "200", description = "Success", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomResponse.class))})
    @ApiResponse(responseCode = "400", description = "Bad Request", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomError.class))})

    @Operation(summary = "GetCliente", description = "Obtiene info del cliente")
    @GetMapping(value = Constants.CLIENTE_METHOD_FIND_PATH)
    public ResponseEntity<?> getCliente(@Valid @PathVariable("clienteid") Integer clienteid) {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(null, OperationType.CLIENTE_GET_REQUEST, globalProperties);

        final ClienteModel data = iClienteUseCase.get(clienteid);
        final ClienteDto clienteDto = clienteMapper.toDto(data);
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(clienteDto);

        LogsHelper.getLogEnd(responseSuccess, OperationType.CLIENTE_GET_REQUEST, globalProperties);
        return ResponseEntity.ok().body(responseSuccess);
    }

    @Operation(summary = "CreateCliente", description = "Registra un nuevo cliente")
    @PostMapping(value = Constants.CLIENTE_METHOD_POST_PATH)
    public ResponseEntity<CustomResponse> createCliente(@RequestBody ClienteCreateRequest clienteCreateRequest) {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(clienteCreateRequest, OperationType.CLIENTE_POST_REQUEST, globalProperties);

        ClienteModel domain = clienteMapper.toDomain(clienteCreateRequest);
        domain = iClienteUseCase.create(domain);
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(domain);

        LogsHelper.getLogEnd(responseSuccess, OperationType.CLIENTE_POST_REQUEST, globalProperties);
        return ResponseEntity.ok().body(responseSuccess);
    }

    @Operation(summary = "UpdateCliente", description = "Actualiza un cliente")
    @PutMapping(value = Constants.CLIENTE_METHOD_PUT_PATH)
    public ResponseEntity<CustomResponse> updateCliente(@RequestBody ClienteUpdateRequest clienteUpdateRequest) {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(clienteUpdateRequest, OperationType.CLIENTE_PUT_REQUEST, globalProperties);

        final ClienteModel domain = clienteMapper.toDomain(clienteUpdateRequest);
        iClienteUseCase.update(domain);
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(domain);

        LogsHelper.getLogEnd(responseSuccess, OperationType.CLIENTE_PUT_REQUEST, globalProperties);
        return ResponseEntity.ok().body(responseSuccess);
    }

    @Operation(summary = "DeleteCliente", description = "Elimina un cliente")
    @DeleteMapping(value = Constants.CLIENTE_METHOD_DELETE_PATH)
    public ResponseEntity<CustomResponse> deleteCliente(@Valid @PathVariable("clienteid") Integer clienteid) {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(null, OperationType.CLIENTE_DELETE_REQUEST, globalProperties);

        iClienteUseCase.delete(clienteid);
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(null);

        LogsHelper.getLogEnd(responseSuccess, OperationType.CLIENTE_DELETE_REQUEST, globalProperties);
        return ResponseEntity.ok().body(responseSuccess);
    }
}