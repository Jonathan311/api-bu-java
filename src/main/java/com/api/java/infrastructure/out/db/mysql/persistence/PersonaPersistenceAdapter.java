package com.api.java.infrastructure.out.db.mysql.persistence;

import com.api.java.application.port.out.IPersonaPersistencePort;
import com.api.java.domain.models.PersonaModel;
import com.api.java.exceptions.ManagerException;
import com.api.java.exceptions.mapping.TypeValidationEnum;
import com.api.java.infrastructure.out.db.mysql.entities.PersonaEntity;
import com.api.java.infrastructure.out.db.mysql.mapper.IPersonaEntityMapper;
import com.api.java.infrastructure.out.db.mysql.repository.IPersonaRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Slf4j
@Component
public class PersonaPersistenceAdapter implements IPersonaPersistencePort {

    @Autowired
    private IPersonaRepository iPersonaRepository;

    @Autowired
    private IPersonaEntityMapper iPersonaEntityMapper;

    @Override
    public PersonaModel get(Integer id) {
        log.info("Start getPersona [{}]", id);
        final Optional<PersonaEntity> personaEntity = iPersonaRepository.findById(id);
        return iPersonaEntityMapper.toDomain(personaEntity.get());
    }

    @Override
    public PersonaModel create(PersonaModel personaModel) {
        try {
            log.info("Start createPersona [{}]", personaModel.protectedToString());
            final PersonaEntity personaEntity = iPersonaEntityMapper.toEntity(personaModel);
            return iPersonaEntityMapper.toDomain(iPersonaRepository.save(personaEntity));
        }catch(Exception ex){
            log.warn("ERROR_CREATE_PERSONA:[{}]", ex.getMessage());
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_DATA_INTEGRITY_VIOLATION_DUPLICATE.name());
        }
    }

    @Override
    public PersonaModel update(PersonaModel personaModel) {
        try {
            log.info("Start updatePersona [{}]", personaModel.protectedToString());
            final PersonaEntity personaEntity = iPersonaEntityMapper.toEntity(personaModel);
            return iPersonaEntityMapper.toDomain(iPersonaRepository.save(personaEntity));
        }catch(Exception ex){
            log.warn("ERROR_UPDATE_PERSONA:[{}]", ex.getMessage());
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_DATA_INTEGRITY_VIOLATION_DUPLICATE.name());
        }
    }

    @Override
    public void delete(PersonaModel personaModel) {
        try {
            log.info("Start deletePersona [{}]", personaModel.protectedToString());
            final PersonaEntity personaEntity = iPersonaEntityMapper.toEntity(personaModel);
            iPersonaRepository.delete(personaEntity);
        }catch(Exception ex){
            log.warn("ERROR_DELETE_PERSONA:[{}]", ex.getMessage());
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_DATA_INTEGRITY_VIOLATION_FOREIGN_KEY.name());
        }
    }
}
