package com.api.java.infrastructure.out.db.mysql.mapper;

import com.api.java.domain.models.MovimientoModel;
import com.api.java.infrastructure.out.db.mysql.entities.MovimientoEntity;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE, nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface IMovimientoEntityMapper {
    MovimientoModel toDomain(MovimientoEntity movimientoEntity);
    List<MovimientoModel> toDomain(List<MovimientoEntity> movimientoEntity);
    MovimientoEntity toEntity(MovimientoModel movimientoModel);
}
