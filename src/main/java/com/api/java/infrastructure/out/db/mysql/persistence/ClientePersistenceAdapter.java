package com.api.java.infrastructure.out.db.mysql.persistence;

import com.api.java.application.port.out.IClientePersistencePort;
import com.api.java.domain.models.ClienteModel;
import com.api.java.exceptions.ManagerException;
import com.api.java.exceptions.mapping.TypeValidationEnum;
import com.api.java.infrastructure.out.db.mysql.entities.ClienteEntity;
import com.api.java.infrastructure.out.db.mysql.mapper.IClienteEntityMapper;
import com.api.java.infrastructure.out.db.mysql.repository.IClienteRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Slf4j
@Component
public class ClientePersistenceAdapter implements IClientePersistencePort {

    @Autowired
    private IClienteRepository iClienteRepository;

    @Autowired
    private IClienteEntityMapper iClienteEntityMapper;

    @Override
    public ClienteModel get(Integer id) {
        log.info("Start getCliente [{}]", id);
        final Optional<ClienteEntity> clienteEntity = iClienteRepository.findById(id);
        if (clienteEntity.isEmpty()) {
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_NOT_FOUND_CLIENT.name());
        }
        return iClienteEntityMapper.toDomain(clienteEntity.get());
    }

    @Override
    public ClienteModel create(ClienteModel clienteModel) {
        try {
            log.info("Start createCliente [{}]", clienteModel.protectedToString());
            final ClienteEntity clienteEntity = iClienteEntityMapper.toEntity(clienteModel);
            return iClienteEntityMapper.toDomain(iClienteRepository.save(clienteEntity));
        } catch (Exception ex) {
            log.warn("ERROR_CREATE_CLIENTE:[{}]", ex.getMessage());
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_DATA_INTEGRITY_VIOLATION_DUPLICATE.name());
        }
    }

    @Override
    public ClienteModel update(ClienteModel clienteModel) {
        try {
            log.info("Start updateCliente [{}]", clienteModel.protectedToString());
            final ClienteEntity clienteEntity = iClienteEntityMapper.toEntity(clienteModel);
            return iClienteEntityMapper.toDomain(iClienteRepository.save(clienteEntity));
        } catch (Exception ex) {
            log.warn("ERROR_UPDATE_CLIENTE:[{}]", ex.getMessage());
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_DATA_INTEGRITY_VIOLATION_DUPLICATE.name());
        }
    }

    @Override
    public void delete(ClienteModel clienteModel) {
        try {
            log.info("Start deleteCliente [{}]", clienteModel.protectedToString());
            final ClienteEntity clienteEntity = iClienteEntityMapper.toEntity(clienteModel);
            iClienteRepository.delete(clienteEntity);
        }catch(Exception ex){
            log.warn("ERROR_DELETE_CLIENTE:[{}]", ex.getMessage());
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_DATA_INTEGRITY_VIOLATION_FOREIGN_KEY.name());
        }
    }
}
