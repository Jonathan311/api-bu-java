package com.api.java.infrastructure.out.db.mysql.mapper;

import com.api.java.domain.models.PersonaModel;
import com.api.java.infrastructure.out.db.mysql.entities.PersonaEntity;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE, nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface IPersonaEntityMapper {
    PersonaModel toDomain(PersonaEntity personaEntity);
    PersonaEntity toEntity(PersonaModel personaModel);
}
