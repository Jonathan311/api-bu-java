package com.api.java.infrastructure.out.db.mysql.repository;

import com.api.java.infrastructure.out.db.mysql.entities.CuentaEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ICuentaRepository extends CrudRepository<CuentaEntity,Integer> {
}