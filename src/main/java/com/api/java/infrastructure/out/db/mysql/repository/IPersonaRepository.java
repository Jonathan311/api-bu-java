package com.api.java.infrastructure.out.db.mysql.repository;

import com.api.java.infrastructure.out.db.mysql.entities.PersonaEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IPersonaRepository extends CrudRepository<PersonaEntity,Integer> {
}