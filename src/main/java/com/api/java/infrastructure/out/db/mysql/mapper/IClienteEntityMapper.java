package com.api.java.infrastructure.out.db.mysql.mapper;

import com.api.java.domain.models.ClienteModel;
import com.api.java.infrastructure.out.db.mysql.entities.ClienteEntity;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE, nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface IClienteEntityMapper {
    ClienteModel toDomain(ClienteEntity clienteEntity);
    ClienteEntity toEntity(ClienteModel clienteModel);
}
