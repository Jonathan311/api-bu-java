package com.api.java.infrastructure.out.db.mysql.persistence;

import com.api.java.application.port.out.IMovimientoPersistencePort;
import com.api.java.domain.models.MovimientoModel;
import com.api.java.exceptions.ManagerException;
import com.api.java.exceptions.mapping.TypeValidationEnum;
import com.api.java.infrastructure.out.db.mysql.entities.MovimientoEntity;
import com.api.java.infrastructure.out.db.mysql.mapper.IMovimientoEntityMapper;
import com.api.java.infrastructure.out.db.mysql.repository.IMovimientoRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Slf4j
@Component
public class MovimientoPersistenceAdapter implements IMovimientoPersistencePort {

    @Autowired
    private IMovimientoRepository iMovimientoRepository;

    @Autowired
    private IMovimientoEntityMapper iMovimientoEntityMapper;

    @Override
    public MovimientoModel get(Integer id) {
        log.info("Start getMovimiento [{}]", id);
        final Optional<MovimientoEntity> movimientoEntity = iMovimientoRepository.findById(id);
        if (movimientoEntity.isEmpty()) {
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_NOT_FOUND_MOVIMIENTO.name());
        }
        return iMovimientoEntityMapper.toDomain(movimientoEntity.get());
    }

    @Override
    public MovimientoModel create(MovimientoModel movimientoModel) {
        try {
            log.info("Start createMovimiento [{}]", movimientoModel.protectedToString());
            final MovimientoEntity movimientoEntity = iMovimientoEntityMapper.toEntity(movimientoModel);
            return iMovimientoEntityMapper.toDomain(iMovimientoRepository.save(movimientoEntity));
        } catch (Exception ex) {
            log.warn("ERROR_CREATE_MOVIMIENTO:[{}]", ex.getMessage());
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_DATA_INTEGRITY_VIOLATION_DUPLICATE.name());
        }
    }

    @Override
    public MovimientoModel update(MovimientoModel clienteModel) {
        try {
            log.info("Start updateMovimiento [{}]", clienteModel.protectedToString());
            final MovimientoEntity movimientoEntity = iMovimientoEntityMapper.toEntity(clienteModel);
            return iMovimientoEntityMapper.toDomain(iMovimientoRepository.save(movimientoEntity));
        } catch (Exception ex) {
            log.warn("ERROR_UPDATE_MOVIMIENTO:[{}]", ex.getMessage());
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_DATA_INTEGRITY_VIOLATION_DUPLICATE.name());
        }
    }

    @Override
    public void delete(MovimientoModel clienteModel) {
        try {
            log.info("Start deleteMovimiento [{}]", clienteModel.protectedToString());
            final MovimientoEntity movimientoEntity = iMovimientoEntityMapper.toEntity(clienteModel);
            iMovimientoRepository.delete(movimientoEntity);
        }catch(Exception ex){
            log.warn("ERROR_DELETE_MOVIMIENTO:[{}]", ex.getMessage());
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_DATA_INTEGRITY_VIOLATION_FOREIGN_KEY.name());
        }
    }

    @Override
    public List<MovimientoModel> report(Date start, Date end, Integer cliente) {
        log.info("Start report");
        final List<MovimientoEntity> movimientoEntity = iMovimientoRepository.report(start, end, cliente);
        if (movimientoEntity.isEmpty()) {
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_NOT_FOUND_MOVIMIENTOS.name());
        }
        return iMovimientoEntityMapper.toDomain(movimientoEntity);
    }
}
