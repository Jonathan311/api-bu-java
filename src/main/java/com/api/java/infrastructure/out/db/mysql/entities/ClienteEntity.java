package com.api.java.infrastructure.out.db.mysql.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Builder
@Data
@Entity
@Table(name = "cliente")
@NoArgsConstructor
@AllArgsConstructor
public class ClienteEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String contrasena;
    private boolean estado;
    @ManyToOne
    @JoinColumn(name = "persona_id")
    private PersonaEntity persona;
}
