package com.api.java.infrastructure.out.db.mysql.mapper;

import com.api.java.domain.models.CuentaModel;
import com.api.java.infrastructure.out.db.mysql.entities.CuentaEntity;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE, nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface ICuentaEntityMapper {
    CuentaModel toDomain(CuentaEntity cuentaEntity);
    CuentaEntity toEntity(CuentaModel cuentaModel);
}
