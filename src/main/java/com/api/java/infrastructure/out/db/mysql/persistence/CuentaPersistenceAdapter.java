package com.api.java.infrastructure.out.db.mysql.persistence;

import com.api.java.application.port.out.ICuentaPersistencePort;
import com.api.java.domain.models.CuentaModel;
import com.api.java.exceptions.ManagerException;
import com.api.java.exceptions.mapping.TypeValidationEnum;
import com.api.java.infrastructure.out.db.mysql.entities.CuentaEntity;
import com.api.java.infrastructure.out.db.mysql.mapper.ICuentaEntityMapper;
import com.api.java.infrastructure.out.db.mysql.repository.ICuentaRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Slf4j
@Component
public class CuentaPersistenceAdapter implements ICuentaPersistencePort {

    @Autowired
    private ICuentaRepository iCuentaRepository;

    @Autowired
    private ICuentaEntityMapper iCuentaEntityMapper;

    @Override
    public CuentaModel get(Integer id) {
        log.info("Start getCuenta [{}]", id);
        final Optional<CuentaEntity> cuentaEntity = iCuentaRepository.findById(id);
        if (cuentaEntity.isEmpty()) {
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_NOT_FOUND_CUENTA.name());
        }
        return iCuentaEntityMapper.toDomain(cuentaEntity.get());
    }

    @Override
    public CuentaModel create(CuentaModel cuentaModel) {
        try {
            log.info("Start createCuenta [{}]", cuentaModel.protectedToString());
            final CuentaEntity cuentaEntity = iCuentaEntityMapper.toEntity(cuentaModel);
            return iCuentaEntityMapper.toDomain(iCuentaRepository.save(cuentaEntity));
        } catch (Exception ex) {
            log.warn("ERROR_CREATE_CUENTA:[{}]", ex.getMessage());
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_DATA_INTEGRITY_VIOLATION_DUPLICATE.name());
        }
    }

    @Override
    public CuentaModel update(CuentaModel cuentaModel) {
        try {
            log.info("Start updateCuenta [{}]", cuentaModel.protectedToString());
            final CuentaEntity cuentaEntity = iCuentaEntityMapper.toEntity(cuentaModel);
            return iCuentaEntityMapper.toDomain(iCuentaRepository.save(cuentaEntity));
        } catch (Exception ex) {
            log.warn("ERROR_UPDATE_CUENTA:[{}]", ex.getMessage());
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_DATA_INTEGRITY_VIOLATION_DUPLICATE.name());
        }
    }

    @Override
    public void delete(CuentaModel cuentaModel) {
        try {
            log.info("Start deleteCuenta [{}]", cuentaModel.protectedToString());
            final CuentaEntity cuentaEntity = iCuentaEntityMapper.toEntity(cuentaModel);
            iCuentaRepository.delete(cuentaEntity);
        }catch(Exception ex){
            log.warn("ERROR_DELETE_CUENTA:[{}]", ex.getMessage());
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_DATA_INTEGRITY_VIOLATION_FOREIGN_KEY.name());
        }
    }
}
