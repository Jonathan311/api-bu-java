package com.api.java.infrastructure.out.db.mysql.repository;

import com.api.java.infrastructure.out.db.mysql.entities.MovimientoEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface IMovimientoRepository extends CrudRepository<MovimientoEntity,Integer> {
    @Query(value = "SELECT m FROM MovimientoEntity m join m.cuenta cu join cu.cliente c WHERE m.fecha BETWEEN :start AND :end AND c.id = :cliente")
    List<MovimientoEntity> report(Date start, Date end, Integer cliente);
}