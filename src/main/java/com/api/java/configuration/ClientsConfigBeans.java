package com.api.java.configuration;

import com.api.java.commons.crypt.CryptHelper;
import com.api.java.commons.crypt.CryptProperties;
import lombok.Data;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.validation.constraints.NotNull;

@Data
@Configuration
public class ClientsConfigBeans {

    @Bean("cryptoHelper")
    public CryptHelper cryptoHelper(@NotNull CryptProperties cryptProperties) {
        return new CryptHelper(cryptProperties.getKey(), cryptProperties.getInitializationVector());
    }
}
