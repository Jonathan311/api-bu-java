package com.api.java.domain.service;

import com.api.java.application.port.in.ICuentaUseCase;
import com.api.java.application.port.out.IClientePersistencePort;
import com.api.java.application.port.out.ICuentaPersistencePort;
import com.api.java.commons.GlobalProperties;
import com.api.java.commons.crypt.CryptHelper;
import com.api.java.domain.models.ClienteModel;
import com.api.java.domain.models.CuentaModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class CuentaService implements ICuentaUseCase {

    @Autowired
    private ICuentaPersistencePort iCuentaPersistencePort;

    @Autowired
    private IClientePersistencePort iClientePersistencePort;

    @Autowired
    private GlobalProperties globalProperties;

    @Autowired
    private CryptHelper cryptoHelper;

    @Override
    public CuentaModel get(Integer id) {
        log.info("Start Domain Service getCuenta:[{}]", id);
        return iCuentaPersistencePort.get(id);
    }

    @Override
    public CuentaModel create(CuentaModel cuentaModel) {
        log.info("Start Domain Service createCuenta:[{}]", cuentaModel.protectedToString());

        ClienteModel clienteModel = iClientePersistencePort.get(cuentaModel.getCliente().getId());
        cuentaModel.setSaldoActual(cuentaModel.getSaldoInicial());
        cuentaModel.setCliente(clienteModel);
        cuentaModel.setEstado(true);
        cuentaModel.setNumeroCuenta(cryptoHelper.encoder(cuentaModel.getNumeroCuenta()));
        cuentaModel = iCuentaPersistencePort.create(cuentaModel);

        log.info("End Domain Service createCuenta");
        return cuentaModel;
    }

    @Override
    public CuentaModel update(CuentaModel cuentaModel) {
        log.info("Start Domain Service updateCuenta:[{}]", cuentaModel.protectedToString());
        iCuentaPersistencePort.get(cuentaModel.getId());
        cuentaModel.setSaldoActual(cuentaModel.getSaldoInicial());
        cuentaModel.setCliente(cuentaModel.getCliente());
        cuentaModel.setNumeroCuenta(cryptoHelper.encoder(cuentaModel.getNumeroCuenta()));
        cuentaModel = iCuentaPersistencePort.update(cuentaModel);

        log.info("End Domain Service updateCuenta");

        return cuentaModel;
    }

    @Override
    public void delete(Integer id) {
        log.info("Start Domain Service deleteCuenta:[{}]", id);
        final CuentaModel cuentaFound = iCuentaPersistencePort.get(id);
        iCuentaPersistencePort.delete(cuentaFound);
        log.info("End Domain Service deleteCuenta");
    }
}
