package com.api.java.domain.service;

import com.api.java.application.port.in.IClienteUseCase;
import com.api.java.application.port.out.IClientePersistencePort;
import com.api.java.application.port.out.IPersonaPersistencePort;
import com.api.java.commons.GlobalProperties;
import com.api.java.commons.crypt.CryptHelper;
import com.api.java.domain.models.ClienteModel;
import com.api.java.domain.models.PersonaModel;
import com.api.java.exceptions.ManagerException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;

@Slf4j
@Service
public class ClienteService implements IClienteUseCase {

    @Autowired
    private IClientePersistencePort iClientePersistencePort;

    @Autowired
    private IPersonaPersistencePort iPersonaPersistencePort;

    @Autowired
    private PlatformTransactionManager transactionManager;

    @Autowired
    private GlobalProperties globalProperties;

    @Autowired
    private CryptHelper cryptoHelper;

    @Override
    public ClienteModel get(Integer id) {
        log.info("Start Domain Service getCliente:[{}]", id);
        return iClientePersistencePort.get(id);
    }

    @Override
    public ClienteModel create(ClienteModel clienteModel) {
        log.info("Start Domain Service createCliente:[{}]", clienteModel.protectedToString());

        TransactionStatus transaction = this.transactionManager.getTransaction(null);

        try {
            PersonaModel personaModel = iPersonaPersistencePort.create(clienteModel.getPersona());
            clienteModel.setPersona(personaModel);
            clienteModel.setEstado(true);
            clienteModel.setContrasena(cryptoHelper.encoder(clienteModel.getContrasena()));
            clienteModel = iClientePersistencePort.create(clienteModel);

            this.transactionManager.commit(transaction);

            return clienteModel;
        } catch (ManagerException ex) {
            this.transactionManager.rollback(transaction);
            throw new ManagerException(ex.getIdentity());
        } finally {
            log.info("End Domain Service createCliente");
        }
    }

    @Override
    public ClienteModel update(ClienteModel clienteModel) {
        log.info("Start Domain Service updateCliente:[{}]", clienteModel.protectedToString());
        final ClienteModel clientFound = iClientePersistencePort.get(clienteModel.getId());

        PersonaModel personaFound = iPersonaPersistencePort.get(clientFound.getPersona().getId());
        clienteModel.getPersona().setId(personaFound.getId());

        TransactionStatus transaction = this.transactionManager.getTransaction(null);

        try {
            iPersonaPersistencePort.update(clienteModel.getPersona());

            clienteModel.setPersona(clienteModel.getPersona());
            clienteModel.setContrasena(cryptoHelper.encoder(clienteModel.getContrasena()));
            iClientePersistencePort.update(clienteModel);

            this.transactionManager.commit(transaction);

            return clienteModel;
        } catch (ManagerException ex) {
            this.transactionManager.rollback(transaction);
            throw new ManagerException(ex.getIdentity());
        } finally {
            log.info("End Domain Service updateCliente");
        }
    }

    @Override
    public void delete(Integer id) {
        log.info("Start Domain Service deleteCliente:[{}]", id);
        final ClienteModel clientFound = iClientePersistencePort.get(id);

        TransactionStatus transaction = this.transactionManager.getTransaction(null);
        try {
            iClientePersistencePort.delete(clientFound);
            iPersonaPersistencePort.delete(clientFound.getPersona());

            this.transactionManager.commit(transaction);
        } catch (ManagerException ex) {
            this.transactionManager.rollback(transaction);
            throw new ManagerException(ex.getIdentity());
        } finally {
            log.info("End Domain Service deleteCliente");
        }
    }
}
