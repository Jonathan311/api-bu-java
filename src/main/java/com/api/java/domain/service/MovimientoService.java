package com.api.java.domain.service;

import com.api.java.application.port.in.IMovimientoUseCase;
import com.api.java.application.port.out.ICuentaPersistencePort;
import com.api.java.application.port.out.IMovimientoPersistencePort;
import com.api.java.commons.GlobalProperties;
import com.api.java.commons.helper.UtilsHelper;
import com.api.java.domain.models.CuentaModel;
import com.api.java.domain.models.MovimientoModel;
import com.api.java.domain.models.ReportModel;
import com.api.java.exceptions.ManagerException;
import com.api.java.exceptions.mapping.TypeValidationEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;

import java.util.*;

import static com.api.java.commons.Constants.DEBITO;

@Slf4j
@Service
public class MovimientoService implements IMovimientoUseCase {

    @Autowired
    private IMovimientoPersistencePort iMovimientoPersistencePort;

    @Autowired
    private ICuentaPersistencePort iCuentaPersistencePort;

    @Autowired
    private PlatformTransactionManager transactionManager;

    @Autowired
    private GlobalProperties globalProperties;

    @Override
    public MovimientoModel get(Integer id) {
        log.info("Start Domain Service getMovimiento:[{}]", id);
        return iMovimientoPersistencePort.get(id);
    }

    @Override
    public MovimientoModel create(MovimientoModel movimientoModel) {
        log.info("Start Domain Service createMovimiento:[{}]", movimientoModel.protectedToString());

        CuentaModel cuentaModel = iCuentaPersistencePort.get(movimientoModel.getCuenta().getId());

        TransactionStatus transaction = this.transactionManager.getTransaction(null);

        try {
            validarSaldo(movimientoModel, cuentaModel, null);

            cuentaModel.setSaldoActual(movimientoModel.getSaldo());
            iCuentaPersistencePort.update(cuentaModel);

            movimientoModel.setCuenta(cuentaModel);
            movimientoModel = iMovimientoPersistencePort.create(movimientoModel);

            this.transactionManager.commit(transaction);

            return movimientoModel;
        } catch (ManagerException ex) {
            this.transactionManager.rollback(transaction);
            throw new ManagerException(ex.getIdentity());
        } finally {
            log.info("End Domain Service createMovimiento");
        }
    }

    @Override
    public MovimientoModel update(MovimientoModel movimientoModel) {
        log.info("Start Domain Service updateMovimiento:[{}]", movimientoModel.protectedToString());
        MovimientoModel movimientoModelOld = iMovimientoPersistencePort.get(movimientoModel.getId());

        CuentaModel cuentaModel = iCuentaPersistencePort.get(movimientoModel.getCuenta().getId());

        TransactionStatus transaction = this.transactionManager.getTransaction(null);

        try {
            validarSaldo(movimientoModel, cuentaModel, movimientoModelOld);

            cuentaModel.setSaldoActual(movimientoModel.getSaldo());
            iCuentaPersistencePort.update(cuentaModel);

            movimientoModel.setCuenta(cuentaModel);

            movimientoModel = iMovimientoPersistencePort.update(movimientoModel);

            this.transactionManager.commit(transaction);

            return movimientoModel;
        } catch (ManagerException ex) {
            this.transactionManager.rollback(transaction);
            throw new ManagerException(ex.getIdentity());
        } finally {
            log.info("End Domain Service updateMovimiento");
        }
    }

    @Override
    public void delete(Integer id) {
        log.info("Start Domain Service deleteMovimiento:[{}]", id);
        final MovimientoModel movimientoFound = iMovimientoPersistencePort.get(id);

        CuentaModel cuentaModel = iCuentaPersistencePort.get(movimientoFound.getCuenta().getId());

        TransactionStatus transaction = this.transactionManager.getTransaction(null);
        try {
            validarSaldo(null, cuentaModel, movimientoFound);

            cuentaModel.setSaldoActual(movimientoFound.getSaldo());
            iCuentaPersistencePort.update(cuentaModel);

            iMovimientoPersistencePort.delete(movimientoFound);

            this.transactionManager.commit(transaction);
        } catch (ManagerException ex) {
            this.transactionManager.rollback(transaction);
            throw new ManagerException(ex.getIdentity());
        } finally {
            log.info("End Domain Service deleteMovimiento");
        }
    }

    @Override
    public List<ReportModel> report(Date start, Date end, Integer clienteId) {
        log.info("Start Domain Service reportMovimiento");
        end = UtilsHelper.dateEnd(end);
        List<MovimientoModel> movimientoModels = iMovimientoPersistencePort.report(start, end, clienteId);

        List<ReportModel> reportModels = new ArrayList<>();

        movimientoModels.forEach(movimientoModel -> {
            ReportModel report = ReportModel.builder()
                    .fecha(UtilsHelper.formatDate(movimientoModel.getFecha()))
                    .cliente(movimientoModel.getCuenta().getCliente().getPersona().getNombre())
                    .numeroCuenta(movimientoModel.getCuenta().getNumeroCuenta())
                    .tipo(movimientoModel.getCuenta().getTipoCuenta())
                    .saldoInicial(movimientoModel.getCuenta().getSaldoInicial())
                    .estado(movimientoModel.getCuenta().isEstado())
                    .movimiento(movimientoModel.getValor())
                    .saldoDisponible(movimientoModel.getCuenta().getSaldoActual())
                    .build();
            reportModels.add(report);
        });

        log.info("End Domain Service reportMovimiento");
        return reportModels;
    }

    private MovimientoModel validarSaldo(MovimientoModel movimientoModel, CuentaModel cuentaModel, MovimientoModel movimientoModelOld) {
        Double saldo = cuentaModel.getSaldoActual();

        if (!Objects.isNull(movimientoModelOld)) {
            if (!movimientoModel.getCuenta().getId().equals(movimientoModelOld.getCuenta().getId())) {
                throw new ManagerException(TypeValidationEnum.HTTP_ERR_MOVIMIENTO_NOT_ACCOUNT.name());
            }
            if (movimientoModelOld.getTipoMovimiento().equals(DEBITO)) {
                saldo -= movimientoModelOld.getValor();
            } else {
                saldo += movimientoModelOld.getValor();
            }
        }

        if (!Objects.isNull(movimientoModel)) {
            if (movimientoModel.getTipoMovimiento().equals(DEBITO)) {
                saldo += movimientoModel.getValor();
            } else {
                saldo -= movimientoModel.getValor();
            }
            movimientoModel.setSaldo(saldo);
        } else {
            movimientoModelOld.setSaldo(saldo);
            return movimientoModelOld;
        }

        if (saldo <= 0) {
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_BALANCE_NOT_AVAILABLE.name());
        }
        return movimientoModel;
    }
}
