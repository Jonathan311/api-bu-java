package com.api.java.domain.models;

import com.api.java.commons.IModel;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ClienteModel extends IModel {
    private Integer id;
    private String contrasena;
    private boolean estado;
    private PersonaModel persona;

    @Override
    public String protectedToString() {
        return toJson("contrasena");
    }
}
