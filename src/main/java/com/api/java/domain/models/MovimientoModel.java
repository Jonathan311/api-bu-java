package com.api.java.domain.models;

import com.api.java.commons.IModel;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MovimientoModel extends IModel {
    private Integer id;
    private Date fecha;
    private String movimiento;
    private String tipoMovimiento;
    private Double valor;
    private Double saldo;
    private CuentaModel cuenta;

    @Override
    public String protectedToString() {
        return toJson();
    }
}
