package com.api.java.domain.models;

import com.api.java.commons.IModel;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CuentaModel extends IModel {
    private Integer id;
    private String numeroCuenta;
    private String tipoCuenta;
    private Double saldoInicial;
    private Double saldoActual;
    private boolean estado;
    private ClienteModel cliente;

    @Override
    public String protectedToString() {
        return toJson("numeroCuenta");
    }
}
