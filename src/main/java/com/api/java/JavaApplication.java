package com.api.java;

import com.api.java.commons.GlobalProperties;
import com.api.java.commons.crypt.CryptProperties;
import com.api.java.exceptions.mapping.MappingResponse;
import com.api.java.commons.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.scheduling.annotation.EnableScheduling;

@Slf4j
@EnableScheduling
@EnableConfigurationProperties({
		GlobalProperties.class,
		CryptProperties.class,
		MappingResponse.class
})
@SpringBootApplication
public class JavaApplication implements ApplicationListener<ContextRefreshedEvent> {

	private final GlobalProperties globalProperties;

	public JavaApplication(GlobalProperties pGlobalProperties) {
		this.globalProperties = pGlobalProperties;
	}

	public static void main(String[] args) {SpringApplication.run(JavaApplication.class, args);}

	public void onApplicationEvent(ContextRefreshedEvent event) {
		log.info(Constants.LONG_LINE);
		log.info(Constants.LOG_START_PROJECT, globalProperties.getName());
		log.info(Constants.LOG_PORT_OF_PROJECT, globalProperties.getRestPort());
		log.info(Constants.LOG_PROJECT_VERSION, globalProperties.getVersion());
		log.info(Constants.LOG_RUN_OK);
		log.info(Constants.LONG_LINE);
	}
}
