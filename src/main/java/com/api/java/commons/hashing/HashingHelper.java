package com.api.java.commons.hashing;

import org.apache.commons.codec.digest.DigestUtils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public final class HashingHelper {

    private HashingHelper(){
        super();
    }

    public static String getHashing(String originalString) {
        try {
            return base64(sha3(md5(originalString)));
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

    private static byte[] md5(String input) throws NoSuchAlgorithmException {
        MessageDigest mDigest = MessageDigest.getInstance("MD5");
        return mDigest.digest(input.getBytes());
    }

    private static byte[] sha3(byte[] input) throws NoSuchAlgorithmException {
        MessageDigest mDigest = MessageDigest.getInstance("SHA3-256");
        return mDigest.digest(input);
    }

    public static String sha3(String originalString) {
        return new DigestUtils("SHA3-256").digestAsHex(originalString);
    }

    private static String base64(byte[] input) {
        byte[] encodedBytes = (Base64.getEncoder()).encode(input);
        return new String(encodedBytes);
    }
}
