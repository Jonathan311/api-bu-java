package com.api.java.commons.helper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class CommandHelper implements Serializable {

    private static final XmlMapper XML_MAPPER;

    static {
        XML_MAPPER = new XmlMapper();
        XML_MAPPER.enable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING, SerializationFeature.WRAP_ROOT_VALUE);
        XML_MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, Boolean.FALSE);
        XML_MAPPER.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, Boolean.TRUE);
    }

    private CommandHelper() {
        super();
    }

    public static <T extends Serializable> String writeAsXML(T request) throws JsonProcessingException {
        return XML_MAPPER.writeValueAsString(request);
    }

    public static <T extends Serializable> T readXML(@NotNull String xmlString, Class<T> response) throws IOException {
        return XML_MAPPER.readValue(safelyCommand(xmlString), response);
    }

    public static String printIgnore(@NotNull String xmlString, String... tags) {
        if (StringUtils.isEmpty(xmlString)) {
            return xmlString;
        }
        xmlString = safelyCommand(xmlString);

        if (tags == null || ArrayUtils.isEmpty(tags)){ return xmlString; }

        for (String s : tags) {
            String tag = "<".concat(s).concat(">([^<]*)</").concat(s).concat(">");
            Pattern pattern = Pattern.compile(tag);
            Matcher matcher = pattern.matcher(xmlString);
            List<String> listMatches = new ArrayList<>();
            while (matcher.find()) {
                listMatches.add(matcher.group(0));
            }

            int longitud;
            for (String se : listMatches) {
                longitud = se.replaceAll("<" + s + ">", "").replaceAll("</" + s + ">", "").length();
                String replaceValue = "<".concat(s).concat(StringUtils.rightPad(">", longitud + 1, "*")).concat("</").concat(s).concat(">");
                xmlString = xmlString.replaceAll(se, replaceValue);
            }
        }

        return xmlString;
    }

    public static String safelyCommand(@NotNull String xmlString) {
        if (StringUtils.isEmpty(xmlString)){ return xmlString; }

        // Remueve los saltos de linea y tabulaciones
        xmlString = xmlString.replaceAll("(\r\n|\n|\r|\t)", "");

        // Escape de caracter '&'
        xmlString = xmlString.replace("&", "&amp;");

        return xmlString;
    }
}
