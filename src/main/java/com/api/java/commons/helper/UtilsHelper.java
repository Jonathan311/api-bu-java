package com.api.java.commons.helper;

import com.api.java.commons.GlobalProperties;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.api.java.commons.Constants;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;
import org.springframework.http.converter.HttpMessageNotReadableException;

import javax.imageio.ImageIO;
import javax.validation.constraints.NotNull;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.net.InetAddress;
import java.text.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.api.java.commons.Constants.*;

@Slf4j
public final class UtilsHelper {

    @Getter
    private static final ObjectMapper JSON_MAPPER;


    static {
        JSON_MAPPER = new ObjectMapper();
        JSON_MAPPER.configure(SerializationFeature.INDENT_OUTPUT, false);
        JSON_MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    private UtilsHelper() {
        // Not is necessary this implementation
    }

    public static String conversionExceptionToString(@NotNull HttpMessageNotReadableException exception) {
        String errorText = "";
        try {
            if (Objects.isNull(exception.getCause())) {
                errorText = exception.getMessage();
            } else {
                if (Objects.isNull(exception.getCause().getCause())) {
                    errorText = exception.getCause().getMessage();
                } else {
                    errorText = exception.getCause().getCause().getMessage();
                }
            }
            return (errorText.split("at ")[0]).trim();
        } catch (Exception ex) {
            return errorText;
        }
    }

    public static String removeScientificNotation(double value) {
        return new DecimalFormat("#.####################################").format(value);
    }

    // Correlative *****************************************************************************************************

    public static String getCorrelative() {
        return MDC.get(Constants.CORRELATIVE_HEADER);
    }

    public static String assignCorrelative(GlobalProperties globalProperties) {
        return assignCorrelative(globalProperties, null);
    }

    public static String assignCorrelative(GlobalProperties globalProperties, String correlation) {
        String cId = correlation;

        if (correlation == null || correlation.isEmpty()) {
            cId = createCorrelative();
        }

        MDC.putCloseable(Constants.CORRELATIVE_HEADER, cId);
        MDC.putCloseable(Constants.COMPONENT_CORRELATIVE, globalProperties.getName());
        return cId;
    }

    public static String createCorrelative() {
        try {
            return GeneratorHelper.correlationId(InetAddress.getLocalHost().getHostAddress());
        } catch (Exception e) {
            return String.valueOf(GeneratorHelper.correlationId());
        }
    }

    //******************************************************************************************************************

    public static String printIgnore(@NotNull String xmlString, String... tags) {
        if (StringUtils.isEmpty(xmlString)) {
            return xmlString;
        } else {
            xmlString = CommandHelper.safelyCommand(xmlString);
            if (tags != null && !ArrayUtils.isEmpty(tags)) {
                String[] var2 = tags;
                int var3 = tags.length;

                for (int var4 = 0; var4 < var3; ++var4) {
                    String s = var2[var4];
                    String tag = "<".concat(s).concat(">([^<]*)</").concat(s).concat(">");
                    Pattern pattern = Pattern.compile(tag);
                    Matcher matcher = pattern.matcher(xmlString);
                    ArrayList listMatches = new ArrayList();

                    while (matcher.find()) {
                        listMatches.add(matcher.group(0));
                    }

                    String se;
                    String replaceValue;
                    for (Iterator var11 = listMatches.iterator(); var11.hasNext(); xmlString = xmlString.replaceAll(se, replaceValue)) {
                        se = (String) var11.next();
                        int longitud = se.replaceAll("<" + s + ">", "").replaceAll("</" + s + ">", "").length();
                        replaceValue = "<".concat(s).concat(StringUtils.rightPad(">", longitud + 1, "*")).concat("</").concat(s).concat(">");
                    }
                }

                return xmlString;
            } else {
                return xmlString;
            }
        }
    }

    public static String protectFields(String jsonText, String... protectFields) {
        String jsonTextReturn = jsonText;
        for (String protectField : protectFields) {
            String regex = QUOTES + protectField + REGEX_REPLACE_JSON_VALUE;
            Matcher m = Pattern.compile(regex).matcher(jsonTextReturn);
            while (m.find()) {
                String match = m.group();
                String replace = m.group().replace(protectField, EMPTY_STRING).replace(QUOTES, EMPTY_STRING).replace(TWO_DOTS, EMPTY_STRING);
                String placeHolder = replace.replaceAll(REGEX_ALL, ASTERISK);
                String ret = match.replace(replace, placeHolder);
                jsonTextReturn = jsonTextReturn.replace(match, ret);
            }
        }
        return jsonTextReturn;
    }

    public static String maskFields(@NotNull String jsonString, String[] tags) {
        if (StringUtils.isEmpty(jsonString)) {
            return jsonString;
        } else {
            jsonString = jsonString.replaceAll("(\r\n|\n|\r|\t)", "");
            if (tags == null) {
                return jsonString;
            } else {
                for (String tag : tags) {
                    jsonString = jsonString.replaceAll("\"".concat(tag).concat("(\\\\)?\"[:]([^,|^}]*)"), "\"".concat(tag).concat("\":\"****\""));
                }
                return jsonString;
            }
        }
    }

    public static byte[] toByteArray(BufferedImage img, String imageFileType) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            ImageIO.write(img, imageFileType, baos);
            return baos.toByteArray();
        } catch (Throwable e) {
            throw new RuntimeException();
        }
    }

    public static ObjectMapper getJsonMapper() {
        ObjectMapper jsonMapper = new ObjectMapper();
        jsonMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        jsonMapper.configure(SerializationFeature.INDENT_OUTPUT, false);
        return jsonMapper;
    }

    public static ObjectMapper getXmlMapper() {
        XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        xmlMapper.configure(SerializationFeature.INDENT_OUTPUT, false);
        return xmlMapper;
    }

    public static String formatDate(Date date) {
        SimpleDateFormat DateFor = new SimpleDateFormat("dd/MM/yyyy");
        return DateFor.format(date);
    }

    public static Date dateEnd(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 1);
        return c.getTime();
    }
}
