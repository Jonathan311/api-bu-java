package com.api.java.commons;

import java.io.Serializable;

public final class Constants  implements Serializable {

    private Constants() {
        super();
    }

    //Yml properties
    public static final String PROJECT_PATH = "${spring.application.root}";
    public static final String PING_PATH = "${spring.application.services.methods.ping.path}";
    public static final String APP_SAFE = "${spring.application.services.safe-url}";

    //Yml controllers name
    public static final String CLIENTE_CONTROLLER_PATH = "${spring.application.services.names.clientes.path}";
    public static final String CLIENTE_METHOD_FIND_PATH = "${spring.application.services.methods.clientes.get}";
    public static final String CLIENTE_METHOD_POST_PATH = "${spring.application.services.methods.clientes.post}";
    public static final String CLIENTE_METHOD_PUT_PATH = "${spring.application.services.methods.clientes.put}";
    public static final String CLIENTE_METHOD_DELETE_PATH = "${spring.application.services.methods.clientes.delete}";

    public static final String CUENTA_CONTROLLER_PATH = "${spring.application.services.names.cuentas.path}";
    public static final String CUENTA_METHOD_FIND_PATH = "${spring.application.services.methods.cuentas.get}";
    public static final String CUENTA_METHOD_POST_PATH = "${spring.application.services.methods.cuentas.post}";
    public static final String CUENTA_METHOD_PUT_PATH = "${spring.application.services.methods.cuentas.put}";
    public static final String CUENTA_METHOD_DELETE_PATH = "${spring.application.services.methods.cuentas.delete}";

    public static final String MOVIMIENTO_CONTROLLER_PATH = "${spring.application.services.names.movimientos.path}";
    public static final String MOVIMIENTO_METHOD_FIND_PATH = "${spring.application.services.methods.movimientos.get}";
    public static final String MOVIMIENTO_METHOD_POST_PATH = "${spring.application.services.methods.movimientos.post}";
    public static final String MOVIMIENTO_METHOD_PUT_PATH = "${spring.application.services.methods.movimientos.put}";
    public static final String MOVIMIENTO_METHOD_DELETE_PATH = "${spring.application.services.methods.movimientos.delete}";
    public static final String MOVIMIENTO_METHOD_REPORT_PATH = "${spring.application.services.methods.movimientos.report}";

    public static final String SPRING_CONFIG_PREFIX = "spring.application";
    public static final String CRYPT_PROPERTIES_PREFIX = "providers.support-auth";

    //Util
    public static final String LONG_LINE = "------------------------------------------------";
    public static final String CORRELATIVE_HEADER = "correlation-id";
    public static final String COMPONENT_CORRELATIVE = "component";
    public static final String EMPTY_STRING = "";
    public static final String LBL_START = "INICIANDO TRANSACCIÓN";

    public static final String QUOTES = "\"";
    public static final String ASTERISK = "*";
    public static final String TWO_DOTS = ":";

    // Regex
    public static final String REGEX_ALL = ".";
    public static final String REGEX_REPLACE_JSON_VALUE = "\".*?:.*?\".*?\"";

    // Logs on start application
    public static final String LBL_RESPONSE_SERVICE = "[{}] RESPUESTA SERVICIO [{}]: [{}]";
    public static final String LBL_REQUEST_TYPE = "[{}] SOLICITUD SERVICIO [{}] PARÁMETROS: {}"; // Service request
    public static final String LOG_THIRD_REQUEST = "[{}] [{}] SOLICITUD: URL: [{}], SOLICITUD: [{}]"; // Third request
    public static final String LBL_RESPONSE = "[{}] [{}] RESPUESTA: [\"{}\"]"; // Third or service response
    public static final String LBL_END = "FINAL DE LA TRANSACCIÓN";
    public static final String LOG_START_PROJECT = "{} Aplicación iniciada";
    public static final String LOG_PORT_OF_PROJECT = "Puerto: {}";
    public static final String LOG_PROJECT_VERSION = "Versión: {}";
    public static final String LOG_RUN_OK = "Lanzamiento [OK]";

    public static final String[] TAGS_IGNORE = {"numeroCuenta", "contrasena"};
    public static final String DEBITO = "D";

}