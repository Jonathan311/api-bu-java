package com.api.java.commons.crypt;

import com.api.java.commons.Constants;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = Constants.CRYPT_PROPERTIES_PREFIX)
public class CryptProperties {
    private String key;
    private String initializationVector;
}
