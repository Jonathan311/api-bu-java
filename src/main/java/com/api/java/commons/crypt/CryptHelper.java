package com.api.java.commons.crypt;

import com.api.java.exceptions.ManagerException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public final class CryptHelper implements Serializable {

    private static final Charset CHARSET = StandardCharsets.UTF_8;
    private static final String SPEC_NAME = "AES";
    private static final String SPEC = "AES/CBC/PKCS5Padding";

    private final SecretKeySpec secretKeySpec;
    private final transient IvParameterSpec ivParameterSpec;

    public CryptHelper(String secretKey, String initVector) {
        secretKeySpec = generateKeyFrom(secretKey);
        ivParameterSpec = generateInitVectorFrom(initVector);
    }

    private SecretKeySpec generateKeyFrom(String secretKey) {
        byte[] key = secretKey.getBytes(CHARSET);
        return new SecretKeySpec(key, SPEC_NAME);
    }

    private IvParameterSpec generateInitVectorFrom(String vector) {
        return new IvParameterSpec(vector.getBytes(CHARSET));
    }

    public String encoder(String strToEncrypt) {
        try {
            Cipher cipher = Cipher.getInstance(SPEC);
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);

            return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes(CHARSET)));
        } catch (InvalidAlgorithmParameterException | InvalidKeyException | NoSuchPaddingException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException e) {
            throw new ManagerException("ENCRYPT", "Encrypt Error");
        }
    }

    public String decoder(String strToDecrypt) {
        try {
            Cipher cipher = Cipher.getInstance(SPEC);
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);

            return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
        } catch (InvalidAlgorithmParameterException | InvalidKeyException | NoSuchPaddingException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException e) {
            throw new ManagerException("DECRYPT", "Decrypt Error");
        }
    }
}