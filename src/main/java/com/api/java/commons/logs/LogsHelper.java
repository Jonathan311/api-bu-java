package com.api.java.commons.logs;

import com.api.java.commons.GlobalProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.api.java.commons.helper.UtilsHelper;
import com.api.java.commons.logs.enums.OperationType;
import com.api.java.commons.Constants;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class LogsHelper {

    @Getter
    private static final ObjectMapper objectMapper;

    static {
        objectMapper = new ObjectMapper();
    }

    private LogsHelper(){
        super();
    }

    public static void getLogStart(Object request, OperationType operationType, GlobalProperties globalProperties) {
        try {
            request = printIgnore(request);
            log.info(Constants.EMPTY_STRING);
            log.info(Constants.LBL_START);
            log.info(Constants.LBL_REQUEST_TYPE, globalProperties.getName().toUpperCase(), operationType.name().toUpperCase(), UtilsHelper.getJSON_MAPPER().writeValueAsString(request));
        } catch (JsonProcessingException e) {
            log.error(e.getMessage());
        }
    }

    public static void getLogEnd(Object response, OperationType operationType, GlobalProperties globalProperties) {
        try {
            response = printIgnore(response);
            if (response instanceof Exception) {
                log.info(Constants.LBL_RESPONSE_SERVICE, globalProperties.getName().toUpperCase(), operationType, ((Exception) response).getMessage());
            } else {
                log.info(Constants.LBL_RESPONSE_SERVICE, globalProperties.getName().toUpperCase(), operationType, UtilsHelper.getJSON_MAPPER().writeValueAsString(response));
            }
            log.info(Constants.LBL_END);
            log.info(Constants.EMPTY_STRING);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage());
        }
    }

    @SneakyThrows
    private static String printIgnore(Object obj) {
        return UtilsHelper.maskFields(objectMapper.writer().writeValueAsString(obj), Constants.TAGS_IGNORE);
    }
}
