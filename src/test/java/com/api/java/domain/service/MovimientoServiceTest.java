package com.api.java.domain.service;

import com.api.java.application.port.in.IMovimientoUseCase;
import com.api.java.application.port.out.ICuentaPersistencePort;
import com.api.java.application.port.out.IMovimientoPersistencePort;
import com.api.java.domain.models.ClienteModel;
import com.api.java.domain.models.CuentaModel;
import com.api.java.domain.models.MovimientoModel;
import com.api.java.domain.models.PersonaModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest
class MovimientoServiceTest {

    @MockBean
    private IMovimientoPersistencePort iMovimientoPersistencePort;

    @MockBean
    private ICuentaPersistencePort iCuentaPersistencePort;

    @Autowired
    private IMovimientoUseCase iMovimientoUseCase;

    @Test
    void createSuccess() {

        PersonaModel personaModel = PersonaModel.builder()
                .id(1)
                .edad(22)
                .genero("M")
                .direccion("direccion")
                .identificacion("1090486306")
                .nombre("Jonathan")
                .telefono("123456789")
                .build();

        ClienteModel clienteModel = ClienteModel.builder()
                .id(1)
                .contrasena("1234")
                .estado(true)
                .persona(personaModel)
                .build();

        CuentaModel cuentaModel = CuentaModel.builder()
                .id(1)
                .numeroCuenta("5263485154")
                .estado(true)
                .cliente(clienteModel)
                .tipoCuenta("Ahorros")
                .saldoActual(100000.0)
                .saldoInicial(100000.0)
                .build();

        MovimientoModel movimientoModel = MovimientoModel.builder()
                .id(1)
                .movimiento("Retiro 500")
                .fecha(new Date())
                .tipoMovimiento("C")
                .cuenta(cuentaModel)
                .valor(50000.0)
                .saldo(50000.0)
                .build();

        given(iCuentaPersistencePort.get(1)).willReturn(cuentaModel);
        given(iMovimientoPersistencePort.create(any(MovimientoModel.class))).willReturn(movimientoModel);
        var mov = iMovimientoUseCase.create(movimientoModel);

        verify(iMovimientoPersistencePort, times(1)).create(any(MovimientoModel.class));
        Assertions.assertNotNull(mov.getId());
    }

    @Test
    void createFail() {
        try {
            MovimientoModel movimientoModel = MovimientoModel.builder()
                    .build();
            iMovimientoUseCase.create(movimientoModel);
        } catch (NullPointerException e) {
            Assertions.assertTrue(Boolean.TRUE);
        }
    }
}