package com.api.java.domain.service;

import com.api.java.application.port.in.ICuentaUseCase;
import com.api.java.application.port.out.IClientePersistencePort;
import com.api.java.application.port.out.ICuentaPersistencePort;
import com.api.java.domain.models.ClienteModel;
import com.api.java.domain.models.CuentaModel;
import com.api.java.domain.models.PersonaModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest
class CuentaServiceTest {

    @MockBean
    private ICuentaPersistencePort iCuentaPersistencePort;

    @MockBean
    private IClientePersistencePort iClientePersistencePort;

    @Autowired
    private ICuentaUseCase iCuentaUseCase;

    @Test
    void createSuccess() {

        PersonaModel personaModel = PersonaModel.builder()
                .id(1)
                .edad(22)
                .genero("M")
                .direccion("direccion")
                .identificacion("1090486306")
                .nombre("Jonathan")
                .telefono("123456789")
                .build();

        ClienteModel clienteModel = ClienteModel.builder()
                .id(1)
                .contrasena("1234")
                .estado(true)
                .persona(personaModel)
                .build();

        CuentaModel cuentaModel = CuentaModel.builder()
                .id(1)
                .numeroCuenta("5263485154")
                .estado(true)
                .cliente(clienteModel)
                .tipoCuenta("Ahorros")
                .saldoActual(100000.0)
                .saldoInicial(100000.0)
                .build();

        given(iClientePersistencePort.get(1)).willReturn(clienteModel);
        given(iCuentaPersistencePort.create(any(CuentaModel.class))).willReturn(cuentaModel);
        var cuenta = iCuentaUseCase.create(cuentaModel);

        verify(iCuentaPersistencePort, times(1)).create(any(CuentaModel.class));
        Assertions.assertNotNull(cuenta.getId());
    }

    @Test
    void createFail() {
        try {
            CuentaModel cuentaModel = CuentaModel.builder()
                    .build();
            iCuentaUseCase.create(cuentaModel);
        } catch (NullPointerException e) {
            Assertions.assertTrue(Boolean.TRUE);
        }
    }
}