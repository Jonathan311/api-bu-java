package com.api.java.domain.service;

import com.api.java.application.port.in.IClienteUseCase;
import com.api.java.application.port.out.IClientePersistencePort;
import com.api.java.application.port.out.IPersonaPersistencePort;
import com.api.java.domain.models.ClienteModel;
import com.api.java.domain.models.PersonaModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest
class ClienteServiceTest {

    @MockBean
    private IPersonaPersistencePort iPersonaPersistencePort;

    @MockBean
    private IClientePersistencePort iClientePersistencePort;

    @Autowired
    private IClienteUseCase iClienteUseCase;

    @Test
    void createSuccess() {

        PersonaModel personaModel = PersonaModel.builder()
                .id(1)
                .edad(22)
                .genero("M")
                .direccion("direccion")
                .identificacion("1090486306")
                .nombre("Jonathan")
                .telefono("123456789")
                .build();

        ClienteModel clienteModel = ClienteModel.builder()
                .id(1)
                .contrasena("1234")
                .estado(true)
                .persona(personaModel)
                .build();

        given(iPersonaPersistencePort.create(any(PersonaModel.class))).willReturn(personaModel);
        given(iClientePersistencePort.create(any(ClienteModel.class))).willReturn(clienteModel);
        var cliente = iClienteUseCase.create(clienteModel);

        verify(iClientePersistencePort, times(1)).create(any(ClienteModel.class));
        Assertions.assertNotNull(cliente.getId());
    }

    @Test
    void createFail() {
        try {
            ClienteModel clienteModel = ClienteModel.builder()
                    .build();
            iClienteUseCase.create(clienteModel);
        } catch (NullPointerException e) {
            Assertions.assertTrue(Boolean.TRUE);
        }
    }
}